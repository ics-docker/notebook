#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

set -e

# Exec the specified command or fall back on bash
if [ $# -eq 0 ]; then
    cmd=( "bash" )
else
    cmd=( "$@" )
fi

# Handle special flags if we're root
if [ $(id -u) == 0 ] ; then

    if [[ "$NB_USER" != "conda" ]]; then
        groupadd -g $NB_GID -o ${NB_GROUP:-${NB_USER}}

        # Remove the user if it already exists (e.g., operator)
        if id -u "$NB_USER" >/dev/null 2>&1; then
            userdel  $NB_USER
        fi

        useradd --home /home/$NB_USER -u $NB_UID -g $NB_GID -G 100 -l $NB_USER

        # if workdir is in /home/conda, cd to /home/$NB_USER
        if [[ "$PWD/" == "/home/conda/"* ]]; then
            newcwd="/home/$NB_USER/${PWD:12}"
            if [[ ! -d "$newcwd" ]]; then
                mkdir "$newcwd"
                chown $NB_UID:$NB_GID "$newcwd"
            fi

            # make sure the home directory has the proper owner
            if ! [[ $(stat --format '%G' "/home/${NB_USER}") != "$NB_USER" ]]; then
                chown $NB_UID:$NB_GID /home/$NB_USER
            fi
            
            echo "Setting CWD to $newcwd"
            cd "$newcwd"
        fi
    fi

    # Make a symlink to the shares directory, if mounted and non-existent
    if [[ -d /shares ]] && [[ ! -e shares ]]; then 
        ln -sf /shares shares
    fi

    # Handle case where provisioned storage does not have the correct permissions by default
    # Ex: default NFS/EFS (no auto-uid/gid)
    if [[ "$CHOWN_HOME" == "1" || "$CHOWN_HOME" == 'yes' ]]; then
        echo "Changing ownership of /home/$NB_USER to $NB_UID:$NB_GID with options '${CHOWN_HOME_OPTS}'"
        chown $CHOWN_HOME_OPTS $NB_UID:$NB_GID /home/$NB_USER
    fi

    # Exec the command as NB_USER with the PATH and the rest of
    # the environment preserved
    echo "Executing the command: ${cmd[@]}"
    exec sudo -E -H -u $NB_USER PATH=$PATH XDG_CACHE_HOME=/home/$NB_USER/.cache "${cmd[@]}"
else
    # Warn if looks like user want to override uid/gid but hasn't
    # run the container as root.
    if [[ ! -z "$NB_UID" && "$NB_UID" != "$(id -u)" ]]; then
        echo 'Container must be run as root to set $NB_UID'
    fi
    if [[ ! -z "$NB_GID" && "$NB_GID" != "$(id -g)" ]]; then
        echo 'Container must be run as root to set $NB_GID'
    fi

    # Execute the command
    echo "Executing the command: ${cmd[@]}"
    exec "${cmd[@]}"
fi
