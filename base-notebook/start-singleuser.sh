#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

set -e

# Start Xvfb to be able to use PyVista
export DISPLAY=:99.0
export PYVISTA_OFF_SCREEN=true
export PYVISTA_USE_PANEL=true
export MESA_GL_VERSION_OVERRIDE=3.2
Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &

# set default ip to 0.0.0.0
if [[ "$NOTEBOOK_ARGS $@" != *"--ip="* ]]; then
  NOTEBOOK_ARGS="--ip=0.0.0.0 $NOTEBOOK_ARGS"
fi

# c.Spawner.default_url = '/lab' doesn't work...
# Force it
NOTEBOOK_ARGS="--SingleUserNotebookApp.default_url=/lab $NOTEBOOK_ARGS"

# Source /etc/profile to set variables defined in /etc/profile.d/*
# (EPICS_CA_ADDR_LIST...)
source /etc/profile

NOTEBOOK_BIN="/opt/conda/envs/jupyter/bin/jupyterhub-singleuser"
. /usr/local/bin/start.sh $NOTEBOOK_BIN $NOTEBOOK_ARGS "$@"
