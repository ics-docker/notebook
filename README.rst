jupyter notebook
================

ESS Jupyter notebook Docker_ image.
The image is built on top of several images:

- base-notebook (base notebook image based on Ubuntu 24.04 with Miniforge --conda/mamba-- Python 3.x)
- python-notebook (add Python 3.12 and Mantid environments)
- julia-notebook (add Julia)
- openxal-notebook (add OpenXAL)

Docker pull command::

    docker pull registry.esss.lu.se/ics-docker/notebook:latest


.. _Docker: https://www.docker.com
