# Instructions to automatically update Python environments

With the following commands, one can update all Python packages in the `jupyter` and `mantid` environments to their latests compatible versions.

From the top directory of the repo:

1. Run `$ python python-notebook/env_file_remove_versions.py` to clear version numbers from the environment file
2. Build the docker image `$ docker build -t python:latest python-notebook --no-cache`
3. Run `$ ./python-notebook/export_conda_env.sh`
4. Run `$ python python-notebook/env_update.py`

Then verify the `jupyter.yml` and `mantid.yml` files before creating a new commit. The scripts create `.bak` backup files.
