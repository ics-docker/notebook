#!/bin/bash

docker run --rm -v $(pwd)/python-notebook:/home/conda/notebooks python bash -c ' \
    conda run -n jupyter conda env export > jupyter_updated.yml ; \
    conda run -n mantid conda env export > mantid_updated.yml'
