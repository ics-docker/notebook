import yaml
import shutil

parent_dir = 'python-notebook/'
for env_file in [parent_dir+'jupyter.yml', parent_dir+'mantid.yml']:
    shutil.copyfile(env_file, env_file+'.bak')

    # Load the environment file
    with open(env_file) as f:
        env_data = yaml.safe_load(f)

    # Update each dependency version
    no_version = []
    pip_no_version = []
    for dep in env_data.get('dependencies', []):
        if isinstance(dep, str):  # Skip conda-formatted dependencies
            package_name = dep.split('=')[0]
            no_version.append(package_name)
        elif isinstance(dep, dict) and dep.get('pip') is not None:
            for pip_dep in dep['pip']:
                package_name = pip_dep.split('==')[0]
                pip_no_version.append(package_name)
        else:
            no_version.append(dep)

    # Update and save the environment file
    env_data['dependencies'] = no_version
    if len(pip_no_version) != 0:
        env_data['dependencies'].append({'pip':  pip_no_version})
    with open(env_file, 'w') as f:
        yaml.dump(env_data, f, sort_keys=False)