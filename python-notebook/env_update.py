import yaml

parent_dir = 'python-notebook/'
for env_file in [parent_dir+'jupyter.yml', parent_dir+'mantid.yml']:
    # Load the environment file
    with open(env_file) as f:
        original_env_data = yaml.safe_load(f)

    # Load the updated environment file
    with open(env_file[:-4]+'_updated.yml') as f:
        updated_env_data = yaml.safe_load(f)

    # Read updated version numbers
    updated_conda_packages = {}
    updated_pip_packages = {}
    for dep in updated_env_data.get('dependencies', []):
        if isinstance(dep, str):
            package_name = dep.split('=')[0]
            package_version = dep.split('=')[1]
            updated_conda_packages[package_name] = package_version
        elif isinstance(dep, dict) and dep.get('pip') is not None:
            for pip_dep in dep['pip']:
                package_name = pip_dep.split('==')[0]
                package_version = pip_dep.split('==')[1]
                updated_pip_packages[package_name] = package_version


    # Update each dependency version
    conda_packages = []
    pip_packages = []
    for dep in original_env_data.get('dependencies', []):
        if isinstance(dep, str):
            package_name = dep.split('=')[0]
            conda_packages.append(f"{package_name}={updated_conda_packages[package_name]}")
        elif isinstance(dep, dict) and dep.get('pip') is not None:
            for pip_dep in dep['pip']:
                package_name = pip_dep.split('==')[0]
                pip_packages.append(f"{package_name}=={updated_pip_packages[package_name]}")
        else:
            conda_packages.append(dep)

    class IndentDumper(yaml.Dumper):
        def increase_indent(self, flow=False, indentless=False):
            return super(IndentDumper, self).increase_indent(flow, False)
        
    # Update and save the environment file
    original_env_data['dependencies'] = conda_packages
    if len(pip_packages) != 0:
        original_env_data['dependencies'].append({'pip':  pip_packages})
    with open(env_file, 'w') as f:
        yaml.dump(original_env_data, f , default_flow_style=False, Dumper=IndentDumper, sort_keys=False)